module smartping

go 1.17

replace github.com/smartping/smartping => /src

require (
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/jakecoffman/cron v0.0.0-20190106200828-7e2009c226a5
	github.com/mattn/go-sqlite3 v1.14.9
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	golang.org/x/net v0.0.0-20211020060615-d418f374d309
)

require (
	github.com/blend/go-sdk v1.20211025.3 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/sys v0.0.0-20210423082822-04245dca01da // indirect
)
