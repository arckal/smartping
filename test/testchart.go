package main

import (
	"encoding/json"
	"fmt"
	"github.com/wcharczuk/go-chart"
	"github.com/wcharczuk/go-chart/drawing"
	"io/ioutil"
	"net/http"
	"os"
	"smartping/src/g"
	"strconv"
	"time"
)

func GraphText(x int, y int, txt string) chart.Renderer {
	f, _ := chart.GetDefaultFont()
	rhart, _ := chart.PNG(300, 130)
	chart.Draw.Text(rhart, txt, x, y, chart.Style{
		FontColor: drawing.ColorBlack,
		FontSize:  10,
		Font:      f,
	})
	return rhart
}

func test1() {
	w, _ := os.Create("graph_" + "1" + ".png")
	url := "http://127.0.0.1:8899/api/ping.json?ip=127.0.0.1"
	config := g.PingStMini{}
	timeout := time.Duration(time.Duration(g.Cfg.Base["Timeout"]) * time.Second)
	client := http.Client{
		Timeout: timeout,
	}
	resp, err := client.Get(url)
	if err != nil {
		GraphText(80, 70, "REQUEST API ERROR").Save(w)
		return
	}
	defer resp.Body.Close()
	if resp.StatusCode == 401 {
		GraphText(80, 70, "401-UNAUTHORIZED").Save(w)
		return
	}
	if resp.StatusCode != 200 {
		GraphText(85, 70, "ERROR CODE "+strconv.Itoa(resp.StatusCode)).Save(w)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	err = json.Unmarshal(body, &config)
	if err != nil {
		GraphText(80, 70, "PARSE DATA ERROR").Save(w)
		return
	}
	Xals := []float64{}
	AvgDelay := []float64{}
	LossPk := []float64{}
	Bkg := []float64{}
	MaxDelay := 0.0
	for i := 0; i < len(config.LossPk); i = i + 1 {
		avg, _ := strconv.ParseFloat(config.AvgDelay[i], 64)
		if MaxDelay < avg {
			MaxDelay = avg
		}
		AvgDelay = append(AvgDelay, avg)
		losspk, _ := strconv.ParseFloat(config.LossPk[i], 64)
		LossPk = append(LossPk, losspk)
		Xals = append(Xals, float64(i))
		Bkg = append(Bkg, 100.0)
	}
	fmt.Println(Xals)
	fmt.Println(AvgDelay)
	graph := chart.Chart{
		//Width:  300 * 3,
		//Height: 130 * 3,
		//Background: chart.Style{
		//	FillColor: drawing.Color{249, 246, 241, 255},
		//},
		//XAxis: chart.XAxis{
		//	Style: chart.Style{
		//		//Show:     true,
		//		FontSize: 20,
		//	},
		//	TickPosition: chart.TickPositionBetweenTicks,
		//	ValueFormatter: func(v interface{}) string {
		//		return config.Lastcheck[int(v.(float64))][11:16]
		//	},
		//},
		//YAxis: chart.YAxis{
		//	Style: chart.Style{
		//		//Show:     true,
		//		FontSize: 20,
		//	},
		//	Range: &chart.ContinuousRange{
		//		Min: 0.0,
		//		Max: 100.0,
		//	},
		//	ValueFormatter: func(v interface{}) string {
		//		if vf, isFloat := v.(float64); isFloat {
		//			return fmt.Sprintf("%0.0f", vf)
		//		}
		//		return ""
		//	},
		//},
		//YAxisSecondary: chart.YAxis{
		//	//NameStyle: chart.StyleShow(),
		//	Style: chart.Style{
		//		//Show:     true,
		//		FontSize: 20,
		//	},
		//	Range: &chart.ContinuousRange{
		//		Min: 0.0,
		//		Max: MaxDelay + MaxDelay/10,
		//	},
		//	ValueFormatter: func(v interface{}) string {
		//		if vf, isFloat := v.(float64); isFloat {
		//			return fmt.Sprintf("%0.0f", vf)
		//		}
		//		return ""
		//	},
		//},
		Series: []chart.Series{
			//chart.ContinuousSeries{
			//	Style: chart.Style{
			//		//Show:        true,
			//		StrokeColor: drawing.Color{249, 246, 241, 255},
			//		FillColor:   drawing.Color{249, 246, 241, 255},
			//	},
			//	XValues: Xals,
			//	YValues: Bkg,
			//},
			chart.ContinuousSeries{
				Style: chart.Style{
					Show:        true,
					StrokeColor: drawing.Color{0, 204, 102, 200},
					FillColor:   drawing.Color{0, 204, 102, 200},
				},
				XValues: Xals,
				YValues: AvgDelay,
				//YAxis:   chart.YAxisSecondary,
			},
			//chart.ContinuousSeries{
			//	Style: chart.Style{
			//		//Show:        true,
			//		StrokeColor: drawing.Color{255, 0, 0, 200},
			//		FillColor:   drawing.Color{255, 0, 0, 200},
			//	},
			//	XValues: Xals,
			//	YValues: LossPk,
			//},
		},
	}
	graph.Render(chart.PNG, w)

}
func main() {
	graph := chart.Chart{
		Series: []chart.Series{
			chart.ContinuousSeries{
				XValues: []float64{1.0, 2.0, 3.0, 4.0},
				YValues: []float64{1.0, 2.0, 3.0, 4.0},
			},
		},
	}

	f, _ := os.Create("graph_" + "test" + ".png")
	graph.Render(chart.PNG, f)
	fmt.Println(graph.GetHeight())
	test1()
	//w, _ := os.Create("graph_"+ "error" +".png")
	//GraphText(80, 70, "REQUEST API ERROR").Save(w)
}
